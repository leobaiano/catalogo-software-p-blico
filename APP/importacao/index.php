<?php
	require_once '1.7.6/Classes/PHPExcel.php';

	$objReader = new PHPExcel_Reader_Excel5();
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load( 'catalogo_2015-09-09_ALTERADO_CATEGORIAS.xls' );
	$objPHPExcel->setActiveSheetIndex(0);

	$slug_categoria = '';
	$slug_software = '';

	for( $linha = 2; $linha <= 73; $linha++ ){
		for( $coluna = 0; $coluna <= 6; $coluna++ ){
			if( $coluna == 1 )
				$name = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
			if( $coluna == 2 )
				$description = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
			if( $coluna == 3 )
				$about = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
			if( $coluna == 4 )
				$url = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
			if( $coluna == 5 )
				$image = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
			if( $coluna == 6 )
				$categoria = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
		}
		if( !isset( $categorias ) || empty( $categorias ) ) {
			$categoria_arr = explode( ',', $categoria );
			foreach( $categoria_arr as $cat ) {
				$slug_categoria = get_slug( $cat );
				$slug_software = get_slug( $name );

				$imagem = get_image( $image, $slug_software );

				$categorias[] = array(
									'id'		=> '',
									'id_check'	=> $slug_categoria,
									'name'		=> $cat,
									'quantity'	=> '',
									'softwares'	=> array(
														array(
															'id'			=> '',
															'id_check'		=> $slug_software,
															'name'			=> $name,
															'image'			=> $imagem,
															'imageLarge'	=> $imagem,
															'description'	=> $description,
															'url'			=> $url,
															'about'			=> $about
														),
												   ),
							  );
			}
		}
		else {
			$categoria_arr = explode( ',', $categoria );
			foreach( $categoria_arr as $cat ) {
				$slug_categoria = get_slug( $cat );
				$slug_software = get_slug( $name );
				$check = busca_valor_chave( $slug_categoria, 'id_check', $categorias );
				$imagem = get_image( $image, $slug_software );
				if( !is_bool( $check ) ){
					$software = array(
									'id'			=> '',
									'id_check'		=> $slug_software,
									'name'			=> $name,
									'image'			=> $imagem,
									'imageLarge'	=> $imagem,
									'description'	=> $description,
									'url'			=> $url,
									'about'			=> $about
								);
					array_push( $categorias[$check]['softwares'], $software );
				} else {
					$m_categorias = array(
									'id'		=> '',
									'id_check'	=> $slug_categoria,
									'name'		=> $cat,
									'quantity'	=> '',
									'softwares'	=> array(
														array(
															'id'			=> '',
															'id_check'		=> $slug_software,
															'name'			=> $name,
															'image'			=> $imagem,
															'imageLarge'	=> $imagem,
															'description'	=> $description,
															'url'			=> $url,
															'about'			=> $about
														),
												   ),
							  );
					array_push( $categorias, $m_categorias );
				}
			}
		}
	}

$i_categoria = 0;
foreach( $categorias as $num_categoria ) {
	$categorias[$i_categoria]['id'] = $i_categoria;
	$i_software = 0;
	foreach ($num_categoria['softwares'] as $num_software) {
		$categorias[$i_categoria]['softwares'][$i_software]['id'] = $i_software;
		$i_software++;
	}
	$i_categoria++;
}

$i_categoria = 0;
foreach( $categorias as $num_categoria ) {
	$quantity = count( $num_categoria['softwares'] );
	$categorias[$i_categoria]['quantity'] = $quantity;
	$i_categoria++;
}


echo '<pre>';
	// print_r( $categorias );
	echo json_encode($categorias);
echo '</pre>';


// Busca valor chave em um array
function busca_valor_chave($valor, $chave, $array){
    $i = 0;
    foreach ($array as $item){
        if (isset($item[$chave]) && $item[$chave] == $valor){
            return  $i;
    	}
        $i++;
    }
    return false;
}

// Gerar slug
function get_slug( $text ) {
	// replace non letter or digits by -
	$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	// trim
	$text = trim($text, '-');

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// lowercase
	$text = strtolower($text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	if ( empty( $text ) ) {
		return 'n-a';
	}

	return $text;
}

function get_image( $url, $slug ) {
	// $extensao = substr( $url, -4 );
	$extensao = '.png';
	// $check = strpos( $extensao, "." );
	// if( $check === false ){
	// 	$extensao = '.' . $extensao;
	// }

	$caminho_int = 'marcas/' . $slug . $extensao;

	// copy($url, $caminho_int);

	return $caminho_int;
}