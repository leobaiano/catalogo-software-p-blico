angular.module('starter.controllers', [])

.controller('TabsController', function($scope, $state) {
  $scope.shouldHide = function() {
        if ($state.current.name == "tab.software" || $state.current.name == "tab.softwares") {
            return true;
        } else {
            return false;
        }
    };

    $scope.shareSft = function(url){
      var link = "mailto:?subject=APP%20Software%20Público%20Brasil&body=Conheça%20o%20software%20no%20Portal%20Software%20Público%20Brasil%20em%20" + url;
      console.log(link);
      window.location.href = link;
    }
})

// Controller da página de abertura
.controller('AberturaCtrl', function($scope, $state) {
  $scope.openCatalogo = function() {
    window.localStorage['viewTutorial'] = "true";
    $state.go('tab.catalogo');
  };
})

// Controller da página de catalogo
.controller('CatalogoCtrl', function($scope, Categorias, $ionicScrollDelegate, $timeout){
  $scope.categorias = Categorias.all();
  $scope.scrollForSoftwares = function(){
    $timeout(function() {
      $ionicScrollDelegate.$getByHandle('handle_softwares').scrollTop();
    });
  }
})

// Controller da página de Softwares
.controller('SoftwaresCtrl', function($scope, $state, $stateParams, Categorias) {
  $scope.softwares = Categorias.get_softwares($stateParams.categoriaId);
})

// Controller da página de Software
.controller('SoftwareCtrl', function($scope, $state, $stateParams, Categorias, $ionicHistory, $timeout, $ionicScrollDelegate) {
  $scope.software = Categorias.get($stateParams.categoriaId, $stateParams.softwareId);
  $scope.closeSoftware = function() {
    $ionicHistory.goBack();
    // $timeout(function() {
    //   $ionicScrollDelegate.$getByHandle('handle_softwares').scrollTop();
    // });
    // $state.go('tab.softwares');
  }
})

.filter('searchFor', function(){
    return function(arr, searchString){
        if(!searchString){
            return arr;
        }

        var result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(arr, function(item){
            var isEmptyFilter = true;
            if(item.about.toLowerCase().indexOf(searchString) !== -1){
            result.push(item);
        }
        });
        return result;
    };
})

// Controle da página FAQ
.controller('FaqCtrl', function($scope, $ionicScrollDelegate) {

  $scope.perguntas = [
    {
      'id': 0,
      'pergunta': 'O que é um Software Público?',
      'resposta': 'O Software Público Brasileiro é um tipo específico de solução que adota um modelo de licença livre para o código-fonte, a proteção da identidade original entre o seu nome, marca, código-fonte, documentação e outros artefatos relacionados, por meio do modelo de Licença Pública de Marca – LPM. Disponível na internet em ambiente virtual público, é considerado um benefício para a sociedade, o mercado e o cidadão, conforme as regras e requisitos previstos no Capítulo II da Instrução Normativa N° 01/2011.',
    },
    {
      'id': 1,
      'pergunta': 'Quem pode disponibilizar um software público?',
      'resposta': 'Os softwares podem ser ofertados tanto por órgãos e entes públicos quanto por entidades da iniciativa privada ou por pessoas físicas que desenvolvem projetos de interesse comum.',
    },
    {
      'id': 2,
      'pergunta': 'Como disponibilizar um software público?',
      'resposta': '<ol><li>1. Leia a Instrução Normativa N° 01/2011.</li><li>2.  Leia o Manual do Ofertante.</li><li>3.  Envie o software: Iniciar a disponibilização do software no ambiente de avaliação AvaliaSPB.</li></ol>Para enviar seu software por meio do AvaliaSPB, é preciso ter cadastro no Portal do SPB. Caso necessite esclarecer dúvidas antes de iniciar o processo, entre em contato com a Coordenação do SPB pelo e-mail admin@softwarepublico.gov.br.',
    },
    {
      'id': 3,
      'pergunta': 'Quais são os requisitos jurídicos obrigatórios?',
      'resposta': '<ol><li>1. Registro do software no Instituto Nacional de Propriedade Industrial – INPI, conforme os princípios e regras previstos na Lei n° 9.609, de 19 de fevereiro de 1998;<li>2.  Uso do modelo de licença CreativeCommons General PublicLicense – GPL ("Licença Pública Geral"), versão 2.0, em português, ou algum outro modelo de licença livre que venha a ser aprovado pelo Órgão Central do SISP; e</li><li>3.  Uso do modelo de Licença Pública de Marca – LPM em relação à proteção da marca do software, conforme previsto nos arts. 34 e 35 da Instrução Normativa N° 01/2011.</li></ol>',
    },
    {
      'id': 4,
      'pergunta': 'Quais são os requisitos técnicos obrigatórios?',
      'resposta': '<ol><li>1. Possuir uma versão suficientemente estável e madura do software que possibilite a sua instalação e utilização em um ambiente de produção;</li><li>2.  Apresentar manual de instalação que contenha, no mínimo, as informações elencadas no Anexo I da Instrução Normativa N° 01/2011 e que permita ao usuário instalar o software sem o auxílio do ofertante de SPB;</li><li>3.  Fornecer o código-fonte do software;</li><li>4.  Fornecer todos os scripts necessários à correta instalação e utilização do software, tais como scripts de configuração e scripts de banco de dados, entre outros;</li><li>5.  Explicitar no manual de instalação as diferenças que possam surgir no procedimento de instalação do software a depender das diversas plataformas suportadas por ele (sistema operacional, banco de dados, servidor de aplicação e demais);</li><li>6.  Especificar, obrigatoriamente, pelo criador do software, no cabeçalho de cada arquivo-fonte, que o software está licenciado pelo modelo de licença GPL ("Licença Pública Geral"), versão 2.0, em português, ou algum outro modelo de licença livre que venha a ser aprovado pelo Órgão Central do SISP.</li></ol>',
    },
    {
      'id': 5,
      'pergunta': 'A aquisição do software público é paga?',
      'resposta': 'Não, a aquisição do software público é gratuita.',
    },
    {
      'id': 6,
      'pergunta': 'Quem pode baixar um software público?',
      'resposta': 'Qualquer pessoa pode realizar o download de um software público.',
    },
    {
      'id': 7,
      'pergunta': 'Como baixar um software público?',
      'resposta': '<ol><li>1. Cadastre-se em portal.softwarepublico.gov.br/account/register</li><li>2. Pesquise o software desejado no Catálogo de Software Público em <a href="https://portal.softwarepublico.gov.br/social/search/software_infos" target="_blank">https://portal.softwarepublico.gov.br/social/search/software_infos</a></li><li>3.  Entre no grupo de discussão do software que deseja</li><li>4.  Baixe o código-fonte.</li></ol>',
    },
  ];

  $scope.toggleGroup = function(group) {
    $ionicScrollDelegate.resize();
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
    $ionicScrollDelegate.resize();
  };
  $scope.isGroupShown = function(group) {
    $ionicScrollDelegate.resize();
    return $scope.shownGroup === group;
  };
})

// Controle da Página de contato
.controller('ContatoCtrl', function($scope, $ionicLoading, $http) {
    $scope.message = {
      'name' : '',
      'email' : '',
      'body' : ''
    };

    
    $scope.finalSubmit = function(message) {

      // $scope.sendEmail = _sendEmail;
      //define the mail params as JSON, hard coded for sample code
      // update JSON to reflect message you want to send
      var mailJSON ={
        "key": "sAKdDcqsBIQ7deWCVyy3wQ",
        "message": {
          "html": "<p>" + message.body + "</p>",
          "text": message.body,
          "subject": "Contato enviado pelo aplicativo Software Público",
          "from_email": message.email,
          "from_name": message.name,
          "to": [
            {
              "email": "admin@softwarepublico.gov.br",
              "name": "Contato",
              "type": "to"
            }
          ],
          "bcc_address": "ljunior2005@gmail.com",
          "important": false,
          "track_opens": null,
          "track_clicks": null,
          "auto_text": null,
          "auto_html": null,
          "inline_css": null,
          "url_strip_qs": null,
          "preserve_recipients": null,
          "view_content_link": null,
          "tracking_domain": null,
          "signing_domain": null,
          "return_path_domain": null
        },
        "async": false,
        "ip_pool": "Main Pool"
      };
      //reference to the Mandrill REST api
      var apiURL = "https://mandrillapp.com/api/1.0/messages/send.json";
      //used to send the email via POST of JSON to Manrdill REST API end-point
      // function _sendEmail() {
        $ionicLoading.show({ template: 'Enviando...', duration: 1500});
        $http.post(apiURL, mailJSON).
          success(function(data, status, headers, config) {
            console.log('successful email send.');
            console.log('status: ' + status);
          }).error(function(data, status, headers, config) {
            console.log('error sending email.');
            console.log('status: ' + status);
          });
      // }
    }
})
.directive('formManager', function($ionicLoading) {
  return {
    restrict : 'A',
    controller : function($scope) {
      $scope.$watch('faleComigoForm.$valid', function() {
        console.log("Form validity changed. Now : " + $scope.faleComigoForm.$valid);
      });
      $scope.submit = function() {
        if($scope.faleComigoForm.$valid) {
          $scope.finalSubmit($scope.message);
        } else {
          $ionicLoading.show({ template: 'Preencha todos os campos', duration: 1500})
        }
      }
    }
  }
})
