// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabsController'
  })

  // Each tab has its own nav history stack:

  // Slide de abertura
  .state('abertura', {
    url: '/abertura',
    templateUrl: 'templates/tab-abertura.html',
    controller: 'AberturaCtrl'
  })

  // Pagina de categorias do catalogo
  .state('tab.catalogo', {
    url: '/catalogo',
    views: {
      'tab-catalogo': {
        templateUrl: 'templates/tab-catalogo.html',
        controller: 'CatalogoCtrl'
      }
    }
  })

  // Página do software
  .state('tab.softwares', {
    url: '/catalogo/:categoriaId',
    views: {
      'tab-catalogo':{
        templateUrl: 'templates/tab-softwares.html',
        controller: 'SoftwaresCtrl'
      }
    }
  })
  
  // Página do software
  .state('tab.software', {
    url: '/catalogo/:categoriaId/:softwareId',
    views: {
      'tab-catalogo':{
        templateUrl: 'templates/tab-software.html',
        controller: 'SoftwareCtrl'
      }
    }
  })

  // Página de FAQ
  .state('tab.faq', {
    url: '/faq',
    views: {
      'tab-faq': {
        templateUrl: 'templates/tab-faq.html',
        controller: 'FaqCtrl'
      }
    }
  })

  .state('tab.contato', {
    url: '/contato',
    views: {
      'tab-contato': {
        templateUrl: 'templates/tab-contato.html',
        controller: 'ContatoCtrl'
      }
    }
  });

  window.localStorage['viewTutorial'] = false;
  if ( window.localStorage['viewTutorial'] === "true" ) {
    $urlRouterProvider.otherwise('/tab/catalogo');
  } else {
    $urlRouterProvider.otherwise('/abertura');
  }

  $ionicConfigProvider.backButton.previousTitleText(false).text('');
});